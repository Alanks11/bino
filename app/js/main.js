document.addEventListener("DOMContentLoaded", function () {
    //===raf fix
    (function () {
        var lastTime = 0;
        var vendors = ['ms', 'moz', 'webkit', 'o'];
        for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
            window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
            window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame'] ||
                window[vendors[x] + 'CancelRequestAnimationFrame'];
        }
        if (!window.requestAnimationFrame)
            window.requestAnimationFrame = function (callback, element) {
                var currTime = new Date().getTime();
                var timeToCall = Math.max(0, 16 - (currTime - lastTime));
                var id = window.setTimeout(function () { callback(currTime + timeToCall); },
                    timeToCall);
                lastTime = currTime + timeToCall;
                return id;
            };
        if (!window.cancelAnimationFrame)
            window.cancelAnimationFrame = function (id) {
                clearTimeout(id);
            };
    }());
    //====Slider
    class Slass {
        constructor(opt) {
            this.wrapper = opt.wrapper === undefined ? '#slider-header' : opt.wrapper;
            this.activeSlide = opt.activeSlide === undefined ? 1 : opt.activeSlide;
            this.pagination = opt.pagination === undefined ? true : opt.pagination;
            this.arrows = opt.arrows === undefined ? true : opt.arrows;
            this.autoplay = opt.autoplay === undefined ? true : opt.autoplay;
            this.autoplayTime = opt.autoplayTime === undefined ? 2000 : opt.autoplayTime;
            this.autoplayReverse = opt.autoplayReverse === undefined ? false : opt.autoplayReverse;
            this.scene = document.querySelector(`${this.wrapper} .scene`);
            this.slides = document.querySelectorAll(`${this.wrapper} .slide`);
            this.touchDrag = opt.touchDrag === undefined ? false : opt.touchDrag;
            this.mouseDrag = opt.mouseDrag === undefined ? false : opt.mouseDrag;
            this.verticalMouseDrag = opt.verticalMouseDrag === undefined ? false : opt.verticalMouseDrag;
            this.verticalTouchDrag = opt.verticalTouchDrag === undefined ? false : opt.verticalTouchDrag;
            this.hoverStop = opt.hoverStop === undefined ? true : opt.hoverStop;
            this.heightEQ = opt.heightEQ === undefined ? true : opt.heightEQ;
            this.mode = opt.mode === undefined ? 'Hor' : opt.mode;
            this.animInNextClass = opt.animInNextClass === undefined ? 'animInNext' + this.mode : opt.animInNextClass;
            this.animOutNextClass = opt.animOutNextClass === undefined ? 'animOutNext' + this.mode : opt.animOutNextClass;
            this.animInPrevClass = opt.animInPrevClass === undefined ? 'animInPrev' + this.mode : opt.animInPrevClass;
            this.animOutPrevClass = opt.animOutPrevClass === undefined ? 'animOutPrev' + this.mode : opt.animOutPrevClass;
            this.animDuration = opt.animDuration === undefined ? 1000 : opt.animDuration;
            this.zActive = opt.zActive === undefined ? 2 : opt.zActive;
            this.zHidden = opt.zHidden === undefined ? 1 : opt.zHidden;
            this.bullets = [];
            this.arrowPrev = null;
            this.arrowNext = null;
            this.previousSlide = null;
        };
        stop(interval) {
            clearInterval(interval);
        };
        drawSlide(id, prev, direction, bullet) {
            this.activeSlide = id;
            this.previousSlide = prev;
            let slide = document.querySelector(`${this.wrapper} .slide[data-id="${id}"]`);
            let slides = this.slides;
            for (let i = 0; i < slides.length; i++) {
                slides[i].style.opacity = `0`;
                slides[i].style.zIndex = this.zHidden;
                slides[i].classList.remove(`${this.animInNextClass}`);
                slides[i].classList.remove(`${this.animOutNextClass}`);
                slides[i].classList.remove(`${this.animInPrevClass}`);
                slides[i].classList.remove(`${this.animOutPrevClass}`);
            };
            slide.style.opacity = 1;
            slide.style.zIndex = this.zActive;
            if (bullet) {
                let prSlide = document.querySelector(`${this.wrapper} .slide[data-id="${prev}"]`);
                prSlide.style.opacity = 1;
                // prSlide.style.visibility = '';
                if (direction === 'fw') {
                    for (let i = 0; i < slides.length; i++) {
                        slides[i].classList.add(`${this.animOutNextClass}`);
                    };
                    slide.classList.remove(`${this.animOutNextClass}`);
                    slide.classList.add(`${this.animInNextClass}`);
                } else if (direction === 'bw') {
                    for (let i = 0; i < slides.length; i++) {
                        slides[i].classList.add(`${this.animOutPrevClass}`);
                    };
                    slide.classList.remove(`${this.animOutPrevClass}`);
                    slide.classList.add(`${this.animInPrevClass}`);
                }
            } else {
                if (direction === 'fw') {
                    if (this.activeSlide > 1) {
                        slides[id - 2].style.opacity = "1";
                        //slides[id - 2].style.visibility = '';
                    } else {
                        slides[slides.length - 1].style.opacity = "1";
                        // slides[slides.length - 1].style.visibility = '';
                    }
                    for (let i = 0; i < slides.length; i++) {
                        slides[i].classList.add(`${this.animOutNextClass}`);
                    };
                    slide.classList.remove(`${this.animOutNextClass}`);
                    slide.classList.add(`${this.animInNextClass}`);
                } else if (direction === 'bw') {
                    if (this.activeSlide < slides.length) {
                        slides[id].style.opacity = "1";
                        //slides[id].style.visibility = '';
                    } else {
                        slides[0].style.opacity = "1";
                        //slides[0].style.visibility = '';
                    }
                    for (let i = 0; i < slides.length; i++) {
                        slides[i].classList.add(`${this.animOutPrevClass}`);
                    };
                    slide.classList.remove(`${this.animOutPrevClass}`);
                    slide.classList.add(`${this.animInPrevClass}`);
                }
            }
            let event = new CustomEvent('drawslide', {
                'detail': {
                    id: id,
                    prev: prev,
                    direction: direction,
                    bullet: bullet
                }
            });
            document.querySelector(`${this.wrapper}`).dispatchEvent(event);
        };
        drawBullets() {
            if (this.pagination) {
                for (let i = 0; i < this.bullets.length; i++) {
                    this.bullets[i].classList.remove('active');
                }
                document.querySelector(`${this.wrapper} .bullet[data-id="${this.activeSlide}"]`).classList.add('active');
            };
        };
        nextSlide() {
            this.previousSlide = this.activeSlide;
            this.activeSlide = this.activeSlide === this.slides.length ? 1 : this.activeSlide + 1;
            this.drawSlide(this.activeSlide, this.previousSlide, 'fw', false);
            if (this.pagination) this.drawBullets();
        };
        prevSlide() {
            this.previousSlide = this.activeSlide;
            this.activeSlide = this.activeSlide === 1 ? this.slides.length : this.activeSlide - 1;
            this.drawSlide(this.activeSlide, this.previousSlide, 'bw', false);
            if (this.pagination) this.drawBullets();
        };
        createPagination() {
            let pagination = document.createElement('div');
            pagination.classList.add('pagination');
            this.scene.appendChild(pagination);
            for (let i = 0; i < this.slides.length; i++) {
                let bullet = document.createElement('div');
                bullet.classList.add('bullet');
                bullet.setAttribute('data-id', i + 1);
                pagination.appendChild(bullet);
                this.bullets.push(bullet);
            };
        };
        createArrows() {
            this.arrowPrev = document.createElement('div');
            this.arrowPrev.classList.add('prev');
            this.scene.appendChild(this.arrowPrev);
            this.arrowNext = document.createElement('div');
            this.arrowNext.classList.add('next');
            this.scene.appendChild(this.arrowNext);
        };
        chooseSlide(e) {
            this.previousSlide = this.activeSlide;
            this.activeSlide = +e.target.dataset.id;
            if (this.activeSlide == this.previousSlide) return;
            let dir = this.activeSlide > this.previousSlide ? 'fw' : 'bw';
            this.drawSlide(this.activeSlide, this.previousSlide, dir, true);
            this.drawBullets();
        };
        heightsEQ(elements, scene) {
            let maxHeight = 0;
            for (let i = 0; i < elements.length; i++) {
                elements[i].style.height = '';
            };
            for (let i = 0; i < elements.length; i++) {
                if (elements[i].offsetHeight > maxHeight) {
                    maxHeight = elements[i].offsetHeight;
                };
            };
            for (let i = 0; i < elements.length; i++) {
                elements[i].style.height = maxHeight + 'px';
            };
            scene.style.height = maxHeight + 'px';
        };
        init() {
            //----IE fix---
            (function () {
                if (typeof window.CustomEvent === "function") return false; //If not IE
                function CustomEvent(event, params) {
                    params = params || { bubbles: false, cancelable: false, detail: undefined };
                    var evt = document.createEvent('CustomEvent');
                    evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
                    return evt;
                };
                CustomEvent.prototype = window.Event.prototype;
                window.CustomEvent = CustomEvent;
            })();
            //----
            if (this.autoplayReverse) {
                if (this.activeSlide == this.slides.length) {
                    this.previousSlide = 1;
                } else {
                    this.previousSlide = this.activeSlide + 1;
                }
            } else {
                if (this.activeSlide == 1) {
                    this.previousSlide = this.slides.length;
                } else {
                    this.previousSlide = this.activeSlide - 1;
                }
            }
            let that = this;
            for (let i = 0; i < this.slides.length; i++) {
                this.slides[i].setAttribute('data-id', i + 1);
                this.slides[i].style.animationDuration = `${this.animDuration}ms`;
            };
            this.drawSlide(this.activeSlide, this.previousSlide, 'fw', false);
            if (this.arrows) {
                this.createArrows();
                this.arrowPrev.addEventListener('click', function () {
                    that.prevSlide();
                });
                this.arrowNext.addEventListener('click', function () {
                    that.nextSlide();
                });
            };
            if (this.pagination) {
                this.createPagination();
                this.drawBullets();
                for (let i = 0; i < this.bullets.length; i++) {
                    this.bullets[i].addEventListener('click', function (e) {
                        that.chooseSlide(e);
                    });
                };
            };
            this.scene.ontouchstart = function (e) {
                this.touchPoint = that.verticalTouchDrag ? e.touches[0].screenY : e.touches[0].screenX;
            };
            if (this.touchDrag) {
                this.scene.ontouchend = function (e) {
                    let endPoint = that.verticalTouchDrag ? e.changedTouches[0].screenY : e.changedTouches[0].screenX;
                    if (endPoint > this.touchPoint + 40) {
                        that.prevSlide();
                    } else if (endPoint < this.touchPoint - 40) {
                        that.nextSlide();
                    };
                };
            };
            if (this.mouseDrag) {
                this.scene.onmousedown = function (e) {
                    this.mouseStart = that.verticalMouseDrag ? e.screenY : e.screenX;
                    this.style.cursor = "grab";
                };
                this.scene.onmouseup = function (e) {
                    this.style.cursor = "";
                    let endPoint = that.verticalMouseDrag ? e.screenY : e.screenX;
                    if (endPoint > this.mouseStart + 40) {
                        that.prevSlide();
                    } else if (endPoint < this.mouseStart - 40) {
                        that.nextSlide();
                    };
                };
            };
            let interval = setInterval(function () {
                if (that.autoplayReverse) {
                    that.prevSlide();
                } else {
                    that.nextSlide();
                }
            }, this.autoplayTime);
            if (!this.autoplay) {
                this.stop(interval);
            };
            if (this.hoverStop && this.autoplay) {
                this.scene.addEventListener('mouseenter', function () {
                    that.stop(interval);
                });
                this.scene.addEventListener('mouseleave', function () {
                    interval = setInterval(function () {
                        if (that.autoplayReverse) {
                            that.prevSlide();
                        } else {
                            that.nextSlide();
                        }
                    }, that.autoplayTime);
                });
            };
            if (this.heightEQ) {
                this.heightsEQ(this.slides, this.scene);
                window.addEventListener('resize', function () {
                    let resizeTimeout;
                    if (!resizeTimeout) {
                        resizeTimeout = setTimeout(function () {
                            resizeTimeout = null;
                            that.heightsEQ(that.slides, that.scene);
                        }, 1000);
                    };
                });
            };
        };
    };
    //---SLIDERS----
    (function () {
        let headerSlider = new Slass({
            wrapper: '#js-header-slider',
            pagination: false,
            autoplay: false,
            autoplayTime: 5000,
            arrows: true,
            mouseDrag: false,
            touchDrag: true,
            hoverStop: false,
            animDuration: 800,
            mode: 'Fade'
        });
        headerSlider.init();
        headerSlider.slides[headerSlider.activeSlide - 1].style.backgroundImage = `url('${headerSlider.slides[headerSlider.activeSlide - 1].dataset.bg}')`;
        document.querySelector('#js-header-slider').addEventListener('drawslide', function (e) {
            headerSlider.slides[e.detail.id - 1].style.backgroundImage = `url('${headerSlider.slides[e.detail.id - 1].dataset.bg}')`;
        });
        let srvSliderText = new Slass({
            wrapper: '#js-srv-slider-txt',
            pagination: true,
            autoplay: false,
            autoplayReverse: true,
            autoplayTime: 3000,
            arrows: false,
            mouseDrag: true,
            touchDrag: true,
            hoverStop: false,
            animDuration: 500,
            mode: 'Vert'
        });
        srvSliderText.init();
        let srvSliderImage = new Slass({
            wrapper: '#js-srv-slider-img',
            pagination: false,
            autoplay: false,
            arrows: false,
            mouseDrag: false,
            touchDrag: false,
            hoverStop: false,
            animDuration: 500,
            heightEQ: false,
            mode: 'Hor'
        });
        srvSliderImage.init();
        srvSliderImage.slides[srvSliderImage.activeSlide - 1].style.backgroundImage = `url('${srvSliderImage.slides[srvSliderImage.activeSlide - 1].dataset.bg}')`;
        document.querySelector('#js-srv-slider-txt').addEventListener('drawslide', function (e) {
            srvSliderImage.drawSlide(e.detail.id, e.detail.prev, e.detail.direction, e.detail.bullet);
            srvSliderImage.slides[e.detail.id - 1].style.backgroundImage = `url('${srvSliderImage.slides[e.detail.id - 1].dataset.bg}')`;
        });
        let caseSliderText = new Slass({
            wrapper: '#js-case-slider-txt',
            pagination: true,
            autoplayReverse: false,
            autoplay: false,
            autoplayTime: 3000,
            arrows: false,
            mouseDrag: true,
            touchDrag: true,
            hoverStop: false,
            heightEQ: true,
            animDuration: 500,
            mode: 'Hor'
        });
        caseSliderText.init();
        let caseSliderImage = new Slass({
            wrapper: '#js-case-slider-img',
            pagination: false,
            autoplay: false,
            arrows: false,
            mouseDrag: false,
            touchDrag: true,
            hoverStop: false,
            animDuration: 500,
            heightEQ: false,
            mode: 'Vert'
        });
        caseSliderImage.init();
        caseSliderImage.slides[caseSliderImage.activeSlide - 1].style.backgroundImage = `url('${caseSliderImage.slides[caseSliderImage.activeSlide - 1].dataset.bg}')`;
        document.querySelector('#js-case-slider-txt').addEventListener('drawslide', function (e) {
            caseSliderImage.drawSlide(e.detail.id, e.detail.prev, e.detail.direction, e.detail.bullet);
            caseSliderImage.slides[e.detail.id - 1].style.backgroundImage = `url('${caseSliderImage.slides[e.detail.id - 1].dataset.bg}')`;
        });
    }());
    //----Statistics-animation---- 
    (function () {
        let items = document.querySelectorAll('.js-progress');
        for (let i = 0; i < items.length; i++) {
            items[i].style.opacity = 0;
        };

        function handler() {
            if (0.8 * window.innerHeight > items[0].getBoundingClientRect().top) {
                incr(items, 3000);
            };
        };
        window.addEventListener('scroll', handler);

        function incr(els, time) {
            window.removeEventListener('scroll', handler);
            for (let i = 0; i < els.length; i++) {
                let from = +els[i].dataset.from;
                let to = +els[i].textContent;
                let step = (to - from) / (time / 16);
                let opStep = 1.5 / (time / 16);
                let acc = opStep;
                let timer = setInterval(function () {
                    els[i].textContent = Math.floor(from);
                    if (els[i].dataset.postfix) {
                        els[i].textContent += els[i].dataset.postfix;
                    };
                    from += step;
                    acc += opStep;
                    els[i].style.opacity = acc;
                    if (to < from) {
                        clearInterval(timer);
                        els[i].textContent = to;
                        if (els[i].dataset.postfix) {
                            els[i].textContent += els[i].dataset.postfix;
                        };
                        els[i].style.opacity = 1;
                    };
                }, 16);
            };
        };
    }());
    //----Form validation---
    (function () {
        let form = document.querySelector('.js-form');
        let inputs = form.querySelectorAll('.js-input');
        let submit = form.querySelector('.js-submit');
        let errorClass = 'error';
        let invalidClass = 'invalid';
        let inputsData = {
            name: {
                input: form.querySelector('.js-input[name="name"]'),
                message: 'Поле не должно быть пустым',
                isValid: function () {
                    return (inputsData.name.input.value.trim().length > 0);
                }
            },
            email: {
                input: form.querySelector('.js-input[name="email"]'),
                message: 'Неверный формат электронной почты',
                isValid: function () {
                    return (inputsData.email.input.value.trim().search(/.+@.+\..+/i) !== -1);
                }
            },
            subject: {
                input: form.querySelector('.js-input[name="subject"]'),
                message: 'Поле не должно быть пустым',
                isValid: function () {
                    return (inputsData.subject.input.value.trim().length > 0);
                }
            },
            message: {
                input: form.querySelector('.js-input[name="message"]'),
                message: 'Длина сообщения - не менее 20 символов',
                isValid: function () {
                    return (inputsData.message.input.value.trim().length >= 20);
                }
            },
        }
        function cleanInputs(inputs) {
            for (let i = 0; i < inputs.length; i++) {
                inputs[i].value = '';
                let blur;
                if (typeof (Event) === 'function') {
                    blur = new Event('blur');
                } else {
                    blur = document.createEvent('Event');
                    blur.initEvent('blur', true, true);
                }
                inputs[i].dispatchEvent(blur);
                inputs[i].oninput = null;
                inputs[i].onchange = null;
            }
        }
        function cleanErrors() {
            for (let i = 0; i < inputs.length; i++) {
                let parent = inputs[i].parentNode;
                let error = parent.querySelector(`.${errorClass}`);
                inputs[i].classList.remove(invalidClass);
                if (error) {
                    parent.removeChild(error);
                }
            }
        }
        function createErrorMessage(element) {
            element.classList.add(invalidClass);
            let error = document.createElement('div');
            error.classList.add(errorClass);
            element.parentNode.appendChild(error);
            error.textContent = inputsData[element.getAttribute('name')].message;
            error.style.display = 'block';
            element.oninput = inputsValidator;
            element.onchange = inputsValidator;
        }
        function inputsValidator() {
            cleanErrors();
            for (let key in inputsData) {
                if (!inputsData[key].isValid()) {
                    createErrorMessage(inputsData[key].input);
                }
            }
        }
        submit.addEventListener('click', function (e) {
            e.preventDefault();
            inputsValidator();
            if (!form.querySelector(`.${errorClass}`)) {
                successFunction();
            }
        });
        function successFunction() {
            cleanInputs(inputs);
            let success = document.querySelector('.client-form__success');
            success.classList.add('client-form__success--active');
            document.querySelector('.js-formSuccess-close').onclick = function () {
                success.classList.remove('client-form__success--active');
            };
            setTimeout(function () {
                success.classList.remove('client-form__success--active');
            }, 3000);
            //some ajax etc...
        };
    }());
    //----Placeholder-label animation----
    (function () {
        let inputs = document.querySelectorAll('.js-input');
        for (let i = 0; i < inputs.length; i++) {
            inputs[i].addEventListener('focus', function () {
                this.parentNode.querySelector('.client-form__label-text').classList.add('client-form__label-text--moved');
            });
            inputs[i].addEventListener('blur', function () {
                if (this.value == '') {
                    this.parentNode.querySelector('.client-form__label-text').classList.remove('client-form__label-text--moved');
                }
            });
        };
    }());
    //----Stats Parallax
    (function () {
        let wrapper = document.querySelector('.stat');
        let bg = document.querySelector('.stat__bg');
        let content = document.querySelector('.stat__inner');
        let workHeight = window.innerHeight + wrapper.clientHeight;
        let step = 40 / workHeight;
        let step2 = 40 / workHeight;
        window.addEventListener('scroll', function () {
            if (wrapper.getBoundingClientRect().top > 0 - wrapper.clientHeight && wrapper.getBoundingClientRect().top < window.innerHeight) {
                bg.style.top = `-${(wrapper.getBoundingClientRect().top + wrapper.clientHeight) * step}%`;
                content.style.top = `${30 + (wrapper.getBoundingClientRect().top + wrapper.clientHeight) * step2}%`;
            };
        });
    }());
    //----Height-equalizations----
    (function (eq) {
        let titles = document.querySelectorAll('.feature__title');
        let texts = document.querySelectorAll('.feature__text');

        function commonEQ() {
            eq(titles);
            if (window.matchMedia("(min-width: 575px)").matches) {
                eq(texts);
            } else {
                for (let i = 0; i < texts.length; i++) {
                    texts[i].style.height = '';
                };
            };
        };
        commonEQ();
        window.addEventListener('resize', function () {
            let resizeTimeout;
            if (!resizeTimeout) {
                resizeTimeout = setTimeout(function () {
                    resizeTimeout = null;
                    commonEQ();
                }, 1000);
            };
        });
    }(heightsEQ));
    //----Header-menu collapse
    (function () {
        let slides = document.querySelectorAll('.hds');
        let body = document.querySelector('body');
        collapseMenu({
            togglerElement: document.querySelector('.js-topmenu-toggle'),
            menuElement: document.querySelector('.menu'),
            menuItemElements: document.querySelectorAll('.js-menu-link'),
            menuOpenedClass: 'menu--opened',
            togglerOpenedClass: 'top__toggle--opened',
            toggleCallback: toggleBodyOverflow
        });
        window.addEventListener('resize', function () {
            document.querySelector('.menu').classList.remove('menu--opened');
            body.style.overflow = '';
            document.querySelector('.js-topmenu-toggle').classList.remove('top__toggle--opened');
            for (let i = 0; i < slides.length; i++) {
                slides[i].classList.remove('hds--dark');
            }
        });

        function toggleBodyOverflow() {
            if (window.matchMedia('(max-width:959px)').matches) {
                if (window.getComputedStyle(body).overflow == 'hidden') {
                    body.style.overflow = '';
                    for (let i = 0; i < slides.length; i++) {
                        slides[i].classList.remove('hds--dark');
                    }
                } else {
                    body.style.overflow = 'hidden';
                    for (let i = 0; i < slides.length; i++) {
                        slides[i].classList.add('hds--dark');
                    }
                }
            }
        }
    }());

    //----- Smooth scroll to sections ----
    (function (scrollToPoint) {
        let links = document.querySelectorAll('.js-menu-link');
        let anchor = document.querySelector('.js-anchor');

        //--anchor-icon
        anchor.addEventListener('click', function (e) {
            e.preventDefault();
            scrollToPoint(this.getAttribute('href'), 500);
        });

        //header-menu-links
        for (let i = 0; i < links.length; i++) {
            links[i].addEventListener('click', function (e) {
                e.preventDefault();
                scrollToPoint(this.getAttribute('href'), 500);
            });
        }
    }(scrollToPoint));
    //----Team-block hovers
    (function () {
        let team = document.querySelectorAll('.js-show-member'),
            descr = 'team__member-description',
            img = 'team__member-imgbox',
            media = "(min-width: 1280px)";
        for (let i = 0; i < team.length; i++) {
            team[i].addEventListener('mouseenter', function () {
                if (window.matchMedia(media).matches) {
                    this.parentNode.querySelector(`.${descr}`).classList.add(`${descr}--opened`);
                    this.parentNode.querySelector(`.${img}`).classList.add(`${img}--opened`);
                };
            });
            team[i].parentNode.addEventListener('mouseleave', function () {
                if (window.matchMedia(media).matches) {
                    this.querySelector(`.${descr}`).classList.remove(`${descr}--opened`);
                    this.querySelector(`.${img}`).classList.remove(`${img}--opened`);
                };
            });
        };
    }());
    //----Pricing-block hovers----
    (function () {
        let cards = document.querySelectorAll('.js-card'),
            cost = 'pricing__cost',
            btn = 'button',
            title = 'card__title';

        function hover() {
            this.querySelector(`.${cost}`).classList.add(`${cost}--primary`);
            this.querySelector(`.${title}`).classList.add(`${title}--primary`);
            this.querySelector(`.${btn}`).classList.add(`${btn}--primary`);
            this.querySelector(`.${btn}`).classList.remove(`${btn}--secondary`);
        };

        function unhover() {
            this.querySelector(`.${cost}`).classList.remove(`${cost}--primary`);
            this.querySelector(`.${title}`).classList.remove(`${title}--primary`);
            this.querySelector(`.${btn}`).classList.remove(`${btn}--primary`);
            this.querySelector(`.${btn}`).classList.add(`${btn}--secondary`);
        };
        for (let i = 0; i < cards.length; i++) {
            cards[i].addEventListener('mouseenter', hover);
            cards[i].addEventListener('mouseleave', unhover);
        };
    }());
    //----UP btn----
    (function (up, show, hide) {
        let btn = document.querySelector('.js-up-btn');
        btn.addEventListener('click', function () {
            up(300);
        });
        window.addEventListener('scroll', function () {
            if (window.pageYOffset > 400) {
                show(btn, 300);
            } else {
                hide(btn, 300);
            }
        });
    }(scrollUp, showEl, hideEl));
    //----Lazyload
    (function () {
        let sections = document.querySelectorAll('.lazy-section');
        let preOffset = 300;
        for (let i = 0; i < sections.length; i++) {
            sections[i].lazyImg = sections[i].querySelectorAll('[data-src]');
            sections[i].lazyBg = sections[i].querySelectorAll('[data-bg]');
        }

        function lazyHandler() {
            window.removeEventListener('scroll', lazyHandler);
            setTimeout(function () {
                window.addEventListener('scroll', lazyHandler);
            }, 300);
            for (let i = 0; i < sections.length; i++) {
                if (sections[i].getBoundingClientRect().top <= window.innerHeight + preOffset) {
                    for (let k = 0; k < sections[i].lazyImg.length; k++) {
                        sections[i].lazyImg[k].setAttribute('src', sections[i].lazyImg[k].dataset.src);
                    }
                    for (let k = 0; k < sections[i].lazyBg.length; k++) {
                        sections[i].lazyBg[k].setAttribute('style', `background-image: url('${sections[i].lazyBg[k].dataset.bg}');`);
                    }
                }
            }
        }
        window.addEventListener('scroll', lazyHandler);
    }());
    //----FUNCTIONS----

    function scrollToPoint(id, speed) {
        let target = document.querySelector(id);
        let offset = target.getBoundingClientRect().top + window.pageYOffset;
        let timeStep = speed / (1000 / 60);
        let animStep = offset / timeStep;
        let startPoint = window.pageYOffset;

        function to() {
            if (target.getBoundingClientRect().top <= 0) {
                window.scrollTo(0, offset);
                return;
            };
            window.scrollTo(0, startPoint);
            startPoint += animStep;
            requestAnimationFrame(to);
        };
        requestAnimationFrame(to);
    }

    function heightsEQ(elements) {
        let maxHeight = 0;
        for (let i = 0; i < elements.length; i++) {
            elements[i].style.height = '';
        };
        for (let i = 0; i < elements.length; i++) {
            if (elements[i].clientHeight > maxHeight) {
                maxHeight = elements[i].clientHeight;
            };
        };
        for (let i = 0; i < elements.length; i++) {
            elements[i].style.height = maxHeight + 'px';
        };
    };

    function collapseMenu(options) {
        options.togglerElement.addEventListener('click', function () {
            options.menuElement.classList.toggle(options.menuOpenedClass);
            this.classList.toggle(options.togglerOpenedClass);
            if (options.toggleCallback) options.toggleCallback();

        });
        for (let i = 0; i < options.menuItemElements.length; i++) {
            options.menuItemElements[i].addEventListener('click', function () {
                options.menuElement.classList.remove(options.menuOpenedClass);
                options.togglerElement.classList.remove(options.togglerOpenedClass);
                if (options.toggleCallback) options.toggleCallback();
            });
        };
    };

    function hideEl(el, time) {
        if (window.getComputedStyle(el).display === 'none') return;
        let startOpacity = +window.getComputedStyle(el).opacity;
        let timeStep = time / (1000 / 60);
        let animStep = startOpacity / timeStep;

        function hide() {
            if (startOpacity <= 0) {
                el.style.opacity = 0;
                el.style.display = 'none';
                return;
            };
            startOpacity -= animStep;
            el.style.opacity = startOpacity;
            requestAnimationFrame(hide);
        };
        requestAnimationFrame(hide);
    };

    function showEl(el, time) {
        if (window.getComputedStyle(el).display !== 'none') return;
        el.style.display = 'block';
        el.style.opacity = 0;
        let startOpacity = 0;
        let timeStep = time / (1000 / 60);
        let animStep = 1 / timeStep;

        function show() {
            if (startOpacity >= 1) {
                el.style.opacity = 1;
                return;
            };
            startOpacity += animStep;
            el.style.opacity = startOpacity;
            requestAnimationFrame(show);
        };
        requestAnimationFrame(show);
    };

    function scrollUp(speed) {
        let timeStep = speed / (1000 / 60);
        let animStep = window.pageYOffset / timeStep;

        function up() {
            if (window.pageYOffset <= 0) {
                window.scrollTo(0, 0);
                return;
            };
            window.scrollTo(0, window.pageYOffset - animStep);
            requestAnimationFrame(up);
        };
        requestAnimationFrame(up);
    };
});